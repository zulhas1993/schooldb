﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassRoomController : ControllerBase
    {
        IClassRoomRepository _classRoomRepository;
        public ClassRoomController(IClassRoomRepository classRoomRepository)
        {
            _classRoomRepository = classRoomRepository;
        }

        // GET: api/ClassRoom
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClassRoom>>> GetClassRooms()
        {
            try
            {
                var classRooms = await _classRoomRepository.GetClassRooms();
                if (classRooms == null)
                {
                    return NotFound();
                }

                return Ok(classRooms);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        // GET: api/ClassRoom/3
        [HttpGet("{id}")]
        public async Task<ActionResult<ClassRoom>> GetClassRoom(int id)
        {
            var classRoom = await _classRoomRepository.GetClassRoom(id);

            if (classRoom == null)
            {
                return NotFound();
            }

            return classRoom;
        }
    }
}
