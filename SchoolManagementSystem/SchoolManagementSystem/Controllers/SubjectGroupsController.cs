﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectGroupsController : ControllerBase
    {
        ISubjectGroupRepository _SubjectGroupRepository;

        public SubjectGroupsController(ISubjectGroupRepository SubjectGroupRepository)
        {
            _SubjectGroupRepository = SubjectGroupRepository;
        }

        // GET: api/SubjectGroups
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SubjectGroup>>> GetSubjectGroups()
        {
            try
            {
                var SubjectGroups = await _SubjectGroupRepository.GetSubjectGroups();
                if (SubjectGroups == null)
                {
                    return NotFound();
                }

                return Ok(SubjectGroups);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // GET: api/SubjectGroups/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SubjectGroup>> GetSubjectGroup(int id)
        {
            var SubjectGroup = await _SubjectGroupRepository.GetSubjectGroup(id);

            if (SubjectGroup == null)
            {
                return NotFound();
            }

            return SubjectGroup;
        }

        //// PUT: api/SubjectGroups/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for
        //// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutSubjectGroup(int id, SubjectGroup subjectGroup)
        //{
        //    if (id != subjectGroup.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(subjectGroup).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!SubjectGroupExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        //// POST: api/SubjectGroups
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for
        //// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPost]
        //public async Task<ActionResult<SubjectGroup>> PostSubjectGroup(SubjectGroup subjectGroup)
        //{
        //    _context.SubjectGroup.Add(subjectGroup);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetSubjectGroup", new { id = subjectGroup.Id }, subjectGroup);
        //}

        //// DELETE: api/SubjectGroups/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<SubjectGroup>> DeleteSubjectGroup(int id)
        //{
        //    var subjectGroup = await _context.SubjectGroup.FindAsync(id);
        //    if (subjectGroup == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.SubjectGroup.Remove(subjectGroup);
        //    await _context.SaveChangesAsync();

        //    return subjectGroup;
        //}

        //private bool SubjectGroupExists(int id)
        //{
        //    return _context.SubjectGroup.Any(e => e.Id == id);
        //}
    }
}
