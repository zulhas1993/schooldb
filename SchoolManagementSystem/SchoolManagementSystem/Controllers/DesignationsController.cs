﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DesignationsController : ControllerBase
    {
        IDesignationRepository _designationRepository;
        public DesignationsController(IDesignationRepository designationRepository)
        {
            _designationRepository = designationRepository;
        }


        // GET: api/Designations
        [HttpGet]
        //[Route("GetDesignations")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Designation>>> GetDesignations()
        {
            try
            {
                var designation = await _designationRepository.GetDesignations();
                if (designation == null)
                {
                    return NotFound();
                }

                return Ok(designation);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }


        // GET: api/Designations
        [HttpGet("{id}")]
        //[Route("GetDesignation")]
        public async Task<ActionResult<Designation>> GetDesignation(int id)
        {
            var designation = await _designationRepository.GetDesignation(id);

            if (designation == null)
            {
                return NotFound();
            }

            return designation;
        }


        // GET: api/Designations
        [HttpPost]
        //[Route("PostDesignation")]
        public async Task<IActionResult> PostDesignation([FromBody] Designation model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var designation = await _designationRepository.SaveDesignation(model);

                    if (designation > 0)
                    {
                        return Ok(designation);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                catch (Exception)
                {

                    return BadRequest();
                }

            }

            return BadRequest();
        }


        // GET: api/Designations
        [HttpPut]
        //[Route("PutDesignation")]
        public async Task<IActionResult> PutDesignation([FromBody] Designation model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _designationRepository.SaveDesignation(model);

                    return Ok("Update Designation");
                }
                catch (Exception ex)
                {
                    if (ex.GetType().FullName == "Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException")
                    {
                        return NotFound();
                    }

                    return BadRequest();
                }
            }

            return BadRequest("Model State is not valid");
        }


        // GET: api/Designations
        [HttpDelete("{id}")]
        //[Route("DeleteDesignation")]
        public async Task<IActionResult> DeleteDesignation(int? id)
        {

            if (id == null)
            {
                return BadRequest("Id not exit");
            }
            try
            {
                await _designationRepository.DeleteDesignation(id);
                return Ok("Delete Success");
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
