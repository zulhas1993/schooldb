﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    [Route("api/[controller]")]
   // [ApiController]
    public class EducationSystemController : ControllerBase
    {
        IEducationSystemRepository _educationSystemRepository;
        public EducationSystemController(IEducationSystemRepository educationSystemRepository)
        {
            _educationSystemRepository = educationSystemRepository;
        }
        // GET: api/EducationSystems
        [HttpGet]
        public IEnumerable<EducationSystem> GetEducationSystems()
        {
            return _educationSystemRepository.GetAll();
        }
    }
}
