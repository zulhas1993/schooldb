﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class PoliceStation
    {
        public int Id { get; set; }
        public string PoliceStationName { get; set; }

        public virtual ICollection<AdmissionApply> AdmissionApply { get; set; }
        public virtual District District { get; set; }
        public virtual ICollection<Guardian> Guardians { get; set; }
        public virtual ICollection<PostOffice> PostOffice { get; set; }
        public virtual ICollection<Staff> Staffs { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}
