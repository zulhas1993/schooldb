﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Guardian : PersonInfo
    {  
        [Key]
       
        public int Id { get; set; }
        //[Required]
        //[Display(Name = "Alternative Guardian Name")]
       // [StringLength(100, ErrorMessage = "The input must be at least 3 characters long.", MinimumLength = 3)]
        public string AltGuardianName { get; set; }
       // [Required]
       // [Display(Name = "Relationship with Alternative Guardian")]
      
       // [StringLength(100, ErrorMessage = "The input must be at least 2 characters long.", MinimumLength = 2)]
        public string RelationOfAltGuardian { get; set; }

        //[Required]
        //[Display(Name = "Alternative Guardian's Contact Number")]
        //[DataType(DataType.PhoneNumber, ErrorMessage = "Invalid Contact Number Format")]
        //[RegularExpression(@"\880[0-9]{11}+", ErrorMessage = "Invalid Contact Number Format")]
        public string AltGuardianPhoneNo { get; set; }
        //[Required (ErrorMessage = "Email Address is Required")]
        //[Display(Name = "Alternative Guardian's Email Address")]
        //[DataType(DataType.EmailAddress , ErrorMessage = "Invalid Email Address")]
        public string AltGuardianEmail { get; set; }
        public virtual ICollection<Student> Students { get; set; }
    }
}
