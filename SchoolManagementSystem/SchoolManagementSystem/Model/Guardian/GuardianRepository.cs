﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class GuardianRepository : IGuardianRepository
    {

        private readonly ApplicationDbContext _context;
        public GuardianRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Add(Guardian guardian)
        {
            throw new NotImplementedException();
        }

        public Guardian Find(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Guardian> GetAll()
        {
            throw new NotImplementedException();
        }

        public Guardian Remove(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Guardian guardian)
        {
            throw new NotImplementedException();
        }
    }
}
