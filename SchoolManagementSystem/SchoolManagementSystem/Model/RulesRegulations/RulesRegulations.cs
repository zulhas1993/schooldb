﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class RulesRegulations
    {
        public int Id { get; set; }
        public string RuleDetails { get; set; }

        public virtual Brunch Brunch { get; set; }
    }
}
