﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Teacher : PersonInfo
    {
        public int Id { get; set; }
        public bool MarritialStatus { get; set; }
        public string FathersName { get; set; }
        public string MothersName { get; set; }
        public virtual Brunch Brunch { get; set; }       
        public virtual ICollection<ClassRoutine> ClassRoutine { get; set; }
        public virtual Designation Designation { get; set; }
        public virtual Subject Subject { get; set; }
    }
}
