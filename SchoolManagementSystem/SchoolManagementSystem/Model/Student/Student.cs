﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Student : PersonInfo
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int RegistrationNo { get; set; }
        [Required]
        public int RollNo { get; set; }
        [Required]
        public string FatherName { get; set; }
        [Required]
        public string FatherOccupation { get; set; }
        [Required]
        public string FatherPhoneNo { get; set; }
        [Required]
        public string MotherName { get; set; }
        [Required]
        public string MotherOccupation { get; set; }
        [Required]
        public string MotherPhone { get; set; }
        [Required]
        public string FormarSchoolName { get; set; }
        public virtual DateTime AdmissionDate { get; set; }
        public virtual SchoolClass SchoolClass { get; set; }
        public virtual Quota Quota { get; set; }
        public virtual ICollection<ExamResult> ExamResult { get; set; }
        public virtual Guardian Guardian { get; set; }
    }
}
