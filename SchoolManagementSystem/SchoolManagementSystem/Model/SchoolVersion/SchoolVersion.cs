﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class SchoolVersion
    {
        public int Id { get; set; }
        public string SchoolVersionName { get; set; }
        public string BookType { get; set; }

        public virtual ICollection<EducationSystem> EducationSystem { get; set; }
    }
}
