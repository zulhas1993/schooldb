﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class EducationSystem
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter Education System Name")]
        public string Name { get; set; }
        [Required]
        public int StudentType { get; set; }
        [Required]
        public virtual Brunch Brunch { get; set; }
        [Required]
        public virtual SchoolVersion SchoolVersion { get; set; }
        [Required]
        public virtual Shift Shift { get; set; }
    }
}
