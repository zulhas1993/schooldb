﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public interface IEducationSystemRepository
    {
        void Add(EducationSystem educationSystem);
        IEnumerable<EducationSystem> GetAll();
        EducationSystem Find(int id);
        EducationSystem Remove(int id);
        void Update(EducationSystem educationSystem);
    }
}
