﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class EducationSystemRepository : IEducationSystemRepository
    {
        private readonly ApplicationDbContext _context;
        public EducationSystemRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(EducationSystem educationSystem)
        {
            throw new NotImplementedException();
        }

        public EducationSystem Find(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<EducationSystem> GetAll()
        {
            return _context.EducationSystem.ToList();
        }

        public EducationSystem Remove(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(EducationSystem educationSystem)
        {
            throw new NotImplementedException();
        }
    }
}
