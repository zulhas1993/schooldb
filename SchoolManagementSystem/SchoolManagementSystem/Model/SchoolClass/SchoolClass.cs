﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class SchoolClass
    {
        public int Id { get; set; }
        public string ClassName { get; set; }

        public virtual ICollection<AdmissionApply> AdmissionApply { get; set; }
        public virtual ICollection<ClassRoom> ClassRoom { get; set; }
        public virtual ICollection<ExamResult> ExamResult { get; set; }
        public virtual ICollection<ExamRoutine> ExamRoutine { get; set; }
        public virtual ICollection<NoticeBoard> NoticeBoard { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<Section> Sections { get; set; }
    }
}
