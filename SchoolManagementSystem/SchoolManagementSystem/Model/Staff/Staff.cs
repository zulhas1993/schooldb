﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Staff : PersonInfo
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string FathersName { get; set; }
        [Required]
        public string MothersName { get; set; }
        [Required]
        public DateTime JoingDate { get; set; }
        public Nullable<DateTime> ResignDate { get; set; }
        [Required]
        public virtual Brunch Brunch { get; set; }
        [Required]
        public virtual Designation Designation { get; set; }
    }
}
