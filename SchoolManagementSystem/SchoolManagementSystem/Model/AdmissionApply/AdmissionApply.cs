﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class AdmissionApply : PersonInfo
    {
        [Key]
        public int Id { get; set; }


        [Required(ErrorMessage = "Enter your father's name")]
        [StringLength(30)]
        public string FatherName { get; set; }

        [Required(ErrorMessage = "Enter your father's occupation")]
        [StringLength(20)]
        public string FatherOccupation { get; set; }

        [Required(ErrorMessage = "Enter your father's phone")]
        [MaxLength(15)]
        [MinLength(11)]
        public string FatherPhone { get; set; }

        [Required(ErrorMessage = "Enter your mother's name")]
        [StringLength(30)]
        public string MotherName { get; set; }

        [Required(ErrorMessage = "Enter your mother's occupation")]
        [StringLength(20)]
        public string MotherOccupation { get; set; }

        [Required(ErrorMessage = "Enter your mother's phone")]
        [MaxLength(15)]
        [MinLength(11)]
        public string MotherPhone { get; set; }

        [StringLength(30)]
        public string FormarSchoolName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ApplingDate { get; set; }

        [Required(ErrorMessage = "Select a Class")]
        public virtual SchoolClass SchoolClass { get; set; }

        [Required(ErrorMessage = "Select an option")]
        public virtual Quota Quota { get; set; }
    }
}
