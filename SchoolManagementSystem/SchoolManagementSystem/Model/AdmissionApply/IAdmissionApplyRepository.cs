﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{

    public interface IAdmissionApplyRepository
    {
        void Add(AdmissionApply admissionApply);
        IEnumerable<AdmissionApply> GetAll();
        AdmissionApply Find(int id);
        AdmissionApply Remove(int id);
        void Update(AdmissionApply admissionApply);
    }

}
