﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class ClassRoom
    {
        public int Id { get; set; }
        public virtual SchoolClass SchoolClass { get; set; }
        public virtual Room Room { get; set; }
        public virtual Section Section { get; set; }
        public virtual Shift Shift { get; set; }             
    }
}
