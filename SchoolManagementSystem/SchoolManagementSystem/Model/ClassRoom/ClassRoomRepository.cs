﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class ClassRoomRepository : IClassRoomRepository
    {
        ApplicationDbContext _context;
        public ClassRoomRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<ClassRoom>> GetClassRooms()
        {
            if (_context != null)
            {
                return await _context.ClassRoom.ToListAsync();
            }
            return null;
        }

        public async Task<ClassRoom> GetClassRoom(int id)
        {
            var classRoom = await _context.ClassRoom.FindAsync(id);

            return classRoom;
        }
        public Task<ClassRoom> CreateClassRoom(ClassRoom classRoom)
        {
            throw new NotImplementedException();
        }
        public Task<ClassRoom> UpdateClassRoom(int id, ClassRoom classRoom)
        {
            throw new NotImplementedException();
        }
        public Task<ClassRoom> DeleteClassRoom(int id)
        {
            throw new NotImplementedException();
        }
    }
}
