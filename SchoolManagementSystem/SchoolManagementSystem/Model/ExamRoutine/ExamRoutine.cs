﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class ExamRoutine
    {
        public int Id { get; set; }
        //[Column(TypeName = "date")]
        public Nullable<System.DateTime> ExamDate { get; set; }
        public bool ActiveStatus { get; set; }
        public virtual SchoolClass SchoolClass { get; set; }
        public virtual Exam Exam { get; set; }
        public virtual Shift Shift { get; set; }
        public virtual Subject Subject { get; set; }

    }
}
