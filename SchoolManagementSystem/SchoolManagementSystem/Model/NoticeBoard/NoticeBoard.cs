﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class NoticeBoard
    {
        public int Id { get; set; }
        public string TopicName { get; set; }
        public string NoticeBody { get; set; }
        public Nullable<System.DateTime> PublishDate { get; set; }

        public virtual Brunch Brunch { get; set; }
        public virtual SchoolClass SchoolClass { get; set; }
    }
}
