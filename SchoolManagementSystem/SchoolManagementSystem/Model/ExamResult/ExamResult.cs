﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class ExamResult
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public Nullable<System.DateTime> ExamPublishDate { get; set; }
        [Required]
        public int TotalScore { get; set; }
        [Required]
        public int PassScore { get; set; }
        [Required]
        public int ObtainScore { get; set; }
        public int HighScore { get; set; }
        public int GrandTotalScore { get; set; }
        [Required]
        public string LatterGrade { get; set; }
        [Required]
        public double GradePoint { get; set; }
        public int Position { get; set; }
        [Required]
        public bool ResultStatus { get; set; }
        public int TotalPresent { get; set; }
        public int TotalAbsent { get; set; }
        public string Note { get; set; }
        [Required]
        public virtual SchoolClass SchoolClass { get; set; }
        [Required]
        public virtual Exam Exam { get; set; }
        [Required]
        public virtual Student Student { get; set; }
        [Required]
        public virtual Subject Subject { get; set; }
    }
}
