﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Holiday
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string HolidayName { get; set; }
        [Required]
        // [Column(TypeName = "date")]

        public Nullable<System.DateTime> StartDate { get; set; }
        [Required]
        //[Column(TypeName = "date")]
        public Nullable<System.DateTime> EndDate { get; set; }
        [Required]
        public Nullable<int> NumberOfDay { get; set; }

        public virtual Brunch Brunch { get; set; }
    }
}
