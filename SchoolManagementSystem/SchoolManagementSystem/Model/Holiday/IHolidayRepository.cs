﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{
    public interface IHolidayRepository
    {
        public Task<IEnumerable<Holiday>> GetHolidays();
        public Task<Holiday> GetHoliday(int id);
        public Task<Holiday> UpdateHoliday(int id, Holiday holiday);
        public Task<Holiday> CreateHoliday(Holiday holiday);
        public Task<Holiday> DeleteHoliday(int id);
    }
}

