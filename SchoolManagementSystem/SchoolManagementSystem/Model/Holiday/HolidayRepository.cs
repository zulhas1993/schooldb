﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{
    public class HolidayRepository : IHolidayRepository
    {
        ApplicationDbContext _context;
        public HolidayRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<Holiday> CreateHoliday(Holiday holiday)
        {
            if (holiday.Id == 0)
            {
                // Add new Holiday
                _context.Holiday.Add(holiday);

            }
            else
            {
                // Find Holiday is already exit in database?
                Holiday dbEntry = _context.Holiday.Find(holiday.Id);

                if (dbEntry != null)
                {
                    //Update that Holiday
                    dbEntry.HolidayName = holiday.HolidayName;
                    dbEntry.Brunch = holiday.Brunch;
                    dbEntry.EndDate = holiday.EndDate;
                    dbEntry.NumberOfDay = holiday.NumberOfDay;  
                    dbEntry.StartDate = holiday.StartDate;
                    _context.Holiday.Update(dbEntry);

                }
            }
            //Commit the transaction
            await _context.SaveChangesAsync();

            return holiday;
        }

        public async Task<Holiday> DeleteHoliday(int id)
        {
            Holiday dbEntry = _context.Holiday.Find(id);

            if (dbEntry != null)
            {
                //Delete that Holiday
                _context.Holiday.Remove(dbEntry);

            }
            //Commit the transaction
            await _context.SaveChangesAsync();

            return dbEntry;
        }

        public async Task<Holiday> GetHoliday(int id)
        {
            var holiday = await _context.Holiday.FindAsync(id);

            return holiday;
        }

        public async Task<IEnumerable<Holiday>> GetHolidays()
        {
            if (_context != null)
            {
                return (IEnumerable<Holiday>)await _context.Holiday.ToListAsync();
            }
            return null;
        }

        public Task<Holiday> UpdateHoliday(int id, Holiday holiday)
        {
            throw new NotImplementedException();
        }
    }
}
