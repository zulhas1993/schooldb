﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class ClassRoutine
    {
        public int Id { get; set; }
        [Required]
        public int DayOfWeek { get; set; }
        [Required]
        //[Column(TypeName = "time")]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh\\:mm}")]
        public Nullable<System.TimeSpan> StartTime { get; set; }
        [Required]
        //[Column(TypeName = "time")]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh\\:mm}")]
        public Nullable<TimeSpan> EndTime { get; set; }
        public string ClassDueation { get; set; }
        public Nullable<int> PeriodNumber { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual Teacher Teacher { get; set; }
        public virtual ClassRoom ClassRoom { get; set; }

    }
}
