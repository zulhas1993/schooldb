﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Brunch
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string BrunchName { get; set; }

        [Required]
        public string Principal { get; set; }


        [Required]
        public string SchoolName { get; set; }

        [Required]
        public string SchoolAuthority { get; set; }

        public virtual ICollection<EducationSystem> EducationSystem { get; set; }
        public virtual ICollection<Exam> Exam { get; set; }
        public virtual ICollection<Holiday> Holidays { get; set; }
        public virtual ICollection<NoticeBoard> NoticeBoard { get; set; }
        public virtual ICollection<Room> Rooms { get; set; }
        public virtual ICollection<RulesRegulations> RulesRegulations { get; set; }
        public virtual ICollection<Staff> Staffs { get; set; }
        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}
