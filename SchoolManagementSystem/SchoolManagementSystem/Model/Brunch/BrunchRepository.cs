﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{
    public class BrunchRepository : IBrunchRepository
    {
        ApplicationDbContext _context;
        public BrunchRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Brunch>> GetBrunchs()
        {
            if (_context != null)
            {
                return await _context.Brunch.ToListAsync();
            }
            return null;
        }

        public async Task<Brunch> GetBrunch(int id)
        {
            var brunch = await _context.Brunch.FindAsync(id);

            return brunch;
        }

        public async Task<int> SaveBrunch(Brunch brunch)
        {
            if (brunch.Id == 0)
            {
                // Add new Brunch
                _context.Brunch.Add(brunch);

            }
            else
            {
                // Find Brunch is already exit in database?
                Brunch dbEntry = _context.Brunch.Find(brunch.Id);

                if (dbEntry != null)
                {
                    //Update that Brunch
                    dbEntry.BrunchName = brunch.BrunchName;
                    dbEntry.Principal = brunch.Principal;
                    _context.Brunch.Update(dbEntry);

                }
            }
            //Commit the transaction
            await _context.SaveChangesAsync();

            return brunch.Id;
        }

        public async Task<Brunch> DeleteBrunch(int? id)
        {
            // Find Brunch is already exit in database?
            Brunch dbEntry = _context.Brunch.Find(id);

            if (dbEntry != null)
            {
                //Delete that Brunch
                _context.Brunch.Remove(dbEntry);

            }
            //Commit the transaction
            await _context.SaveChangesAsync();

            return dbEntry;
        }
    }
}
