﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolManagementSystem.Model
{
    public class PersonInfo
    {
        [Required(ErrorMessage = "Enter your First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Enter your Last Name")]
        public string LastName { get; set; }

        public string FullName { get { return FirstName + " " + LastName; } }
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        [Required]
        [StringLength(8, MinimumLength = 4, ErrorMessage = "Use 4-8 characters")]
        [DataType(DataType.Password)]
        [NotMapped]
        public string Password { get; set; }
        [Required(ErrorMessage = "Password not match")]
        [DataType(DataType.Password)]
        [NotMapped]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        public string PasswordHint { get; set; }
        [Required]
        public int Gender { get; set; }
        [Required]
        public int Religion { get; set; }
        [Required]
        public string PhoneNo { get; set; }
        [Required(ErrorMessage = "Enter your Valid Email Address")]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string NidNo { get; set; }        
        public string ImageUrl { get; set; }
        public string CurrentStatus { get; set; }
        [Required]
        public DateTime CreatedDate { get {return DateTime.Now; } }
        [Required]
        public string PresentAddress { get; set; }
        [Required]
        public string ParmanentAddress { get; set; }
        [Required]
        public virtual Country Country { get; set; }
        [Required]
        public virtual District District { get; set; }
        [Required]
        public virtual Division Division { get; set; }
        [Required]
        public virtual PoliceStation PoliceStation { get; set; }
        [Required]
        public virtual PostOffice PostOffice { get; set; }
        
    }
}