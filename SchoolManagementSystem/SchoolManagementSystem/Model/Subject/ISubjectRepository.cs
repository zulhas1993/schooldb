﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public interface ISubjectRepository
    {
        void Add(Subject subject);
        IEnumerable<Subject> GetAll();
        Subject Find(int id);
        Subject Remove(int id);
        void Update(Subject subject);
    }
}
