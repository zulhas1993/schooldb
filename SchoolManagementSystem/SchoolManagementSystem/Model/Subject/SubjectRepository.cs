﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class SubjectRepository : ISubjectRepository
    {

        private readonly ApplicationDbContext _context;
        public SubjectRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Add(Subject subject)
        {
            throw new NotImplementedException();
        }

        public Subject Find(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Subject> GetAll()
        {
            throw new NotImplementedException();
        }

        public Subject Remove(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Subject subject)
        {
            throw new NotImplementedException();
        }
    }
}
