﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Subject
    {  [Key]
        public int Id { get; set; }
        //[Required]
        //[Display(Name = "Name of Subject")]
        //[StringLength(100, ErrorMessage = "The Subject name must be at least 3 characters long.", MinimumLength = 3)]
        public string SubjectName { get; set; }
        //[Required]
        //[Display(Name = "Subject Code")]
        //[DataType(DataType.PhoneNumber, ErrorMessage = "Invalid Subject Code")]
        //[StringLength(3, ErrorMessage = "The Subject code must be at 3 characters long.", MinimumLength = 3)]
        public string SubjectCode { get; set; }


        public virtual ICollection<ClassRoutine> ClassRoutine { get; set; }
        public virtual ICollection<ExamResult> ExamResult { get; set; }
        public virtual ICollection<ExamRoutine> ExamRoutine { get; set; }
        public virtual SubjectGroup SubjectGroup { get; set; }
        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}
