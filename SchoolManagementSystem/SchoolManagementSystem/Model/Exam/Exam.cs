﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Exam
    {
        public int Id { get; set; }
        public string ExamType { get; set; }
        public string ExamDiscription { get; set; }
 
        //[Column(TypeName = "date")]
        public System.DateTime StartDate { get; set; }
        //[Column(TypeName = "date")]
        public System.DateTime EndDate { get; set; }
        public string Duration { get; set; }
        public virtual Brunch Brunch { get; set; }
        public virtual ICollection<ExamResult> ExamResult { get; set; }
        public virtual ICollection<ExamRoutine> ExamRoutine { get; set; }
    }
}
