﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Quota
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter your quota name")]
        [StringLength(20)]
        public string QuotaName { get; set; }

        public virtual ICollection<AdmissionApply> AdmissionApply { get; set; }
        public virtual ICollection<Student> Students { get; set; }
    }
}
