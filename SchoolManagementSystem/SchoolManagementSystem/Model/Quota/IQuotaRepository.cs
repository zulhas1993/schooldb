﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{

    public interface IQuotaRepository
    {
        void Add(Quota quota);
        IEnumerable<Quota> GetAll();
        Quota Find(int id);
        Quota Remove(int id);
        void Update(Quota quota);
    }

}
