﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Section
    {
        public int Id { get; set; }
        public string SectionName { get; set; }


        public virtual ICollection<ClassRoom> ClassRoom { get; set; }
        public virtual SubjectGroup SubjectGroup { get; set; }
        public virtual SchoolClass SchoolClass { get; set; }
    }
}
