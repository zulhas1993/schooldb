﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Shift
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter your shift name")]
        [StringLength(20)]
        public string ShiftName { get; set; }


        public virtual ICollection<ClassRoom> ClassRoom { get; set; }
        public virtual ICollection<EducationSystem> EducationSystem { get; set; }
        public virtual ICollection<ExamRoutine> ExamRoutine { get; set; }
    }
}
