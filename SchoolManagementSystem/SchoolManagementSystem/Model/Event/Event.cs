﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Event
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter  Event Name")]
        public string EventName { get; set; }
        
        [Required(ErrorMessage = "Enter Event starting date")]
        [Column(TypeName = "date")]
        public Nullable<System.DateTime> StartDate { get; set; }
        [Required(ErrorMessage = "Enter Event Ending date")]
        [Column(TypeName = "date")]
        public Nullable<System.DateTime> EndDate { get; set; }
        [Required(ErrorMessage = "Enter  Event Controller Name")]
        public string EventControlar { get; set; }
        public string ImageUrl { get; set; }
        public virtual Brunch Brunch { get; set; }
    }
}
