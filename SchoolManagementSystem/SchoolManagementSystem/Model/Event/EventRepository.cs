﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{
    public class EventRepository : IEventRepository
    {

        ApplicationDbContext _context;
        public EventRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<Event> CreateEvent(Event eve)
        {
            throw new NotImplementedException();
        }

        public async Task<Event> DeleteEvent(int id)
        {
            Event dbEntry = _context.Event.Find(id);

            if (dbEntry != null)
            {
                //Delete that Event
                _context.Event.Remove(dbEntry);

            }
            //Commit the transaction
            await _context.SaveChangesAsync();

            return dbEntry;
            
        }

        public async Task<Event> GetEvent(int id)
        {
            var eve = await _context.Event.FindAsync(id);

            return eve;
        }

        public async Task<IEnumerable<Event>> GetEvents()
        {
            if (_context != null)
            {
                return (IEnumerable<Event>)await _context.Event.ToListAsync();
            }
            return null;
        }

        public async Task<int> SaveEvent(Event eve)
        {
            if (eve.Id == 0)
            {
                // Add new Event
                _context.Event.Add(eve);

            }
            else
            {
                // Find Event is already exit in database?
                Event dbEntry = _context.Event.Find(eve.Id);

                if (dbEntry != null)
                {
                    //Update that Event
                    dbEntry.EventName = eve.EventName;
                    dbEntry.Brunch = eve.Brunch;
                    dbEntry.EndDate = eve.EndDate;
                    dbEntry.EventControlar = eve.EventControlar;
                    dbEntry.ImageUrl = eve.ImageUrl;
                    dbEntry.StartDate = eve.StartDate;
                    _context.Event.Update(dbEntry);

                }
            }
            //Commit the transaction
            await _context.SaveChangesAsync();

            return eve.Id;

        }

        public  Task<Event> UpdateEvent(int id, Event eve)
        {
            throw new NotImplementedException();
        }
    }


}
