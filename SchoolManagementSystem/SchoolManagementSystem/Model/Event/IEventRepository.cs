﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{
    public interface IEventRepository
    {
        public Task<IEnumerable<Event>> GetEvents();
        public Task<Event> GetEvent(int id);
        Task<int> SaveEvent(Event eve);
        public Task<Event> UpdateEvent(int id, Event eve);
        public Task<Event> CreateEvent(Event eve);
        public Task<Event> DeleteEvent(int id);
    }
}
