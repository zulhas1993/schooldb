﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{
    public class Room
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter your Room Name")]
        [Display(Name = " Room Name")]
        public string RoomName { get; set; }
        [Required(ErrorMessage = "Enter Sit Capacity of Room")]
        [Display(Name = "Set Capacity")]
        public int SitCapacity { get; set; }

        [Required(ErrorMessage = "Enter your brunch Name")]
        [Display(Name = "Brunch Name")]
        public virtual Brunch Brunch { get; set; }
        public virtual ICollection<ClassRoom> ClassRoom { get; set; }
    }
}
