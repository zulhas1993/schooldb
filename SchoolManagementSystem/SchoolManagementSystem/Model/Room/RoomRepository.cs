﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Model;


namespace SchoolManagementSystem
{
    public class RoomRepository : IRoomRepository
    {

        ApplicationDbContext _context;
        public RoomRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<Room> CreateRoom(Room room)
        {
            if (room.Id == 0)
            {
                // Add new Room
                _context.Room.Add(room);

            }
            else
            {
                // Find Room is already exit in database?
                Room dbEntry = _context.Room.Find(room.Id);

                if (dbEntry != null)
                {
                    //Update that Room
                    dbEntry.RoomName = room.RoomName;
                    dbEntry.Brunch = room.Brunch;
                   // dbEntry.ClassRoom = room.ClassRoom;
                    dbEntry.SitCapacity = room.SitCapacity;
                    
                    _context.Room.Update(dbEntry);

                }
            }
            //Commit the transaction
            await _context.SaveChangesAsync();

            return room;
        }

        public async Task<Room> DeleteRoom(int id)
        {
            Room dbEntry = _context.Room.Find(id);

            if (dbEntry != null)
            {
                //Delete that Room
                _context.Room.Remove(dbEntry);

            }
            //Commit the transaction
            await _context.SaveChangesAsync();

            return dbEntry;
        }

        public async Task<Room> GetRoom(int id)
        {
            var room = await _context.Room.FindAsync(id);

            return room;
        }

        public async Task<IEnumerable<Room>> GetRooms()
        {
            if (_context != null)
            {
                return await _context.Room.ToListAsync();
            }
            return null;
        }

        public Task<ClassRoom> UpdateRoom(int id, Room room)
        {
            throw new NotImplementedException();
        }

        Task<Room> IRoomRepository.CreateRoom(Room room)
        {
            throw new NotImplementedException();
        }

        Task<Room> IRoomRepository.DeleteRoom(int id)
        {
            throw new NotImplementedException();
        }

        Task<Room> IRoomRepository.UpdateRoom(int id, Room room)
        {
            throw new NotImplementedException();
        }
    }
}
