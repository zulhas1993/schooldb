﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public interface ISubjectGroupRepository
    {
        public Task<IEnumerable<SubjectGroup>> GetSubjectGroups();
        public Task<SubjectGroup> GetSubjectGroup(int id);
        public Task<SubjectGroup> UpdateSubjectGroup(int id, SubjectGroup subjectGroup);
        public Task<SubjectGroup> CreateSubjectGroup(SubjectGroup subjectGroup);
        public Task<SubjectGroup> DeleteSubjectGroup(int id);
    }
}

