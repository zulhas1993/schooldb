﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class SubjectGroup
    {  
        [Key]
        public int Id { get; set; }
        //[Required]
        //[Display(Name = "Name of Group")]
        //[StringLength(100, ErrorMessage = "The Group name must be at least 3 characters long.", MinimumLength = 3)]
        public string GroupName { get; set; }

        public virtual ICollection<Section> Sections { get; set; }
        public virtual ICollection<Subject> Subjects { get; set; }
    }
}
