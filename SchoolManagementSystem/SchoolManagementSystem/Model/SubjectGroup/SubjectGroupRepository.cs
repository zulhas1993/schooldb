﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class SubjectGroupRepository : ISubjectGroupRepository
    {
         ApplicationDbContext _context;
        public SubjectGroupRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<SubjectGroup> CreateSubjectGroup(SubjectGroup subjectGroup)
        {
            //if (_context != null)
            //{
            //    return await _context.SubjectGroup.ToListAsync();
            //}
            //return null;
            throw new NotImplementedException();
        }

        public Task<SubjectGroup> DeleteSubjectGroup(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<SubjectGroup> GetSubjectGroup(int id)
        {
            var subjectGroup = await _context.SubjectGroup.FindAsync(id);

            return subjectGroup;
        }

        public async Task<IEnumerable<SubjectGroup>> GetSubjectGroups()
        {
            if (_context != null)
            {
                return await _context.SubjectGroup.ToListAsync();
            }
            return null;
        }

        public Task<SubjectGroup> UpdateSubjectGroup(int id, SubjectGroup subjectGroup)
        {
            throw new NotImplementedException();
        }
    }
}
