﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolManagementSystem.Migrations
{
    public partial class UpdateExamTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Exam_SchoolClass_SchoolClassId",
                table: "Exam");

            migrationBuilder.DropIndex(
                name: "IX_Exam_SchoolClassId",
                table: "Exam");

            migrationBuilder.DropColumn(
                name: "SchoolClassId",
                table: "Exam");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SchoolClassId",
                table: "Exam",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Exam_SchoolClassId",
                table: "Exam",
                column: "SchoolClassId");

            migrationBuilder.AddForeignKey(
                name: "FK_Exam_SchoolClass_SchoolClassId",
                table: "Exam",
                column: "SchoolClassId",
                principalTable: "SchoolClass",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
