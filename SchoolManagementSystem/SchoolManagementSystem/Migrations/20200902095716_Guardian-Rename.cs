﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolManagementSystem.Migrations
{
    public partial class GuardianRename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Students_Gardian_GardianId",
                table: "Students");

            migrationBuilder.DropTable(
                name: "Gardian");

            migrationBuilder.DropIndex(
                name: "IX_Students_GardianId",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "GardianId",
                table: "Students");

            migrationBuilder.AddColumn<int>(
                name: "GuardianId",
                table: "Students",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DesignationName",
                table: "Designation",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SchoolName",
                table: "Brunch",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SchoolAuthority",
                table: "Brunch",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Principal",
                table: "Brunch",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BrunchName",
                table: "Brunch",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FatherName",
                table: "AdmissionApply",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Guardian",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    PasswordHint = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Religion = table.Column<int>(nullable: false),
                    PhoneNo = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    NidNo = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    CurrentStatus = table.Column<string>(nullable: true),
                    PresentAddress = table.Column<string>(nullable: true),
                    ParmanentAddress = table.Column<string>(nullable: true),
                    CountryId = table.Column<int>(nullable: true),
                    DistrictId = table.Column<int>(nullable: true),
                    DivisionId = table.Column<int>(nullable: true),
                    PoliceStationId = table.Column<int>(nullable: true),
                    PostOfficeId = table.Column<int>(nullable: true),
                    AltGuardianName = table.Column<string>(nullable: true),
                    RelationOfAltGuardian = table.Column<string>(nullable: true),
                    AltGuardianPhoneNo = table.Column<string>(nullable: true),
                    AltGuardianEmail = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Guardian", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Guardian_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Guardian_District_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "District",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Guardian_Division_DivisionId",
                        column: x => x.DivisionId,
                        principalTable: "Division",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Guardian_PoliceStation_PoliceStationId",
                        column: x => x.PoliceStationId,
                        principalTable: "PoliceStation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Guardian_PostOffice_PostOfficeId",
                        column: x => x.PostOfficeId,
                        principalTable: "PostOffice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Students_GuardianId",
                table: "Students",
                column: "GuardianId");

            migrationBuilder.CreateIndex(
                name: "IX_Guardian_CountryId",
                table: "Guardian",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Guardian_DistrictId",
                table: "Guardian",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Guardian_DivisionId",
                table: "Guardian",
                column: "DivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Guardian_PoliceStationId",
                table: "Guardian",
                column: "PoliceStationId");

            migrationBuilder.CreateIndex(
                name: "IX_Guardian_PostOfficeId",
                table: "Guardian",
                column: "PostOfficeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Students_Guardian_GuardianId",
                table: "Students",
                column: "GuardianId",
                principalTable: "Guardian",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Students_Guardian_GuardianId",
                table: "Students");

            migrationBuilder.DropTable(
                name: "Guardian");

            migrationBuilder.DropIndex(
                name: "IX_Students_GuardianId",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "GuardianId",
                table: "Students");

            migrationBuilder.AddColumn<int>(
                name: "GardianId",
                table: "Students",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DesignationName",
                table: "Designation",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "SchoolName",
                table: "Brunch",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "SchoolAuthority",
                table: "Brunch",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Principal",
                table: "Brunch",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "BrunchName",
                table: "Brunch",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "FatherName",
                table: "AdmissionApply",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20);

            migrationBuilder.CreateTable(
                name: "Gardian",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AltGardianEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AltGardianName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AltGardianPhoneNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CountryId = table.Column<int>(type: "int", nullable: true),
                    CurrentStatus = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DistrictId = table.Column<int>(type: "int", nullable: true),
                    DivisionId = table.Column<int>(type: "int", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Gender = table.Column<int>(type: "int", nullable: false),
                    ImageUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NidNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ParmanentAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PasswordHint = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PoliceStationId = table.Column<int>(type: "int", nullable: true),
                    PostOfficeId = table.Column<int>(type: "int", nullable: true),
                    PresentAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RelationOfAltGardian = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Religion = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gardian", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Gardian_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Gardian_District_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "District",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Gardian_Division_DivisionId",
                        column: x => x.DivisionId,
                        principalTable: "Division",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Gardian_PoliceStation_PoliceStationId",
                        column: x => x.PoliceStationId,
                        principalTable: "PoliceStation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Gardian_PostOffice_PostOfficeId",
                        column: x => x.PostOfficeId,
                        principalTable: "PostOffice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Students_GardianId",
                table: "Students",
                column: "GardianId");

            migrationBuilder.CreateIndex(
                name: "IX_Gardian_CountryId",
                table: "Gardian",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Gardian_DistrictId",
                table: "Gardian",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Gardian_DivisionId",
                table: "Gardian",
                column: "DivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Gardian_PoliceStationId",
                table: "Gardian",
                column: "PoliceStationId");

            migrationBuilder.CreateIndex(
                name: "IX_Gardian_PostOfficeId",
                table: "Gardian",
                column: "PostOfficeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Students_Gardian_GardianId",
                table: "Students",
                column: "GardianId",
                principalTable: "Gardian",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
