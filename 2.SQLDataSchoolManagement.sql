use SchoolManagementDB;
go
--01
SET IDENTITY_INSERT [dbo].[Brunch] ON 

INSERT [dbo].[Brunch] ([Id], [BrunchName], [Principal], [SchoolName], [SchoolAuthority]) VALUES (1, N'Mirpur', N'Jahidul Hasan', N'Round41', N'Mahadi')
INSERT [dbo].[Brunch] ([Id], [BrunchName], [Principal], [SchoolName], [SchoolAuthority]) VALUES (2, N'Cumilla', N'Robiul', N'Round41', N'Imran')
INSERT [dbo].[Brunch] ([Id], [BrunchName], [Principal], [SchoolName], [SchoolAuthority]) VALUES (3, N'Khilgaw', N'Imran', N'Round41', N'Alauddin Japor')
INSERT [dbo].[Brunch] ([Id], [BrunchName], [Principal], [SchoolName], [SchoolAuthority]) VALUES (4, N'Keranigang', N'Akram', N'Round41', N'Kashem')
SET IDENTITY_INSERT [dbo].[Brunch] OFF
go
--02
SET IDENTITY_INSERT [dbo].[SchoolVersion] ON 

INSERT [dbo].[SchoolVersion] ([Id], [SchoolVersionName], [BookType]) VALUES (1, N'Bangla', N'Bangla')
INSERT [dbo].[SchoolVersion] ([Id], [SchoolVersionName], [BookType]) VALUES (2, N'Bangla', N'English')
INSERT [dbo].[SchoolVersion] ([Id], [SchoolVersionName], [BookType]) VALUES (3, N'English', N'English')
INSERT [dbo].[SchoolVersion] ([Id], [SchoolVersionName], [BookType]) VALUES (4, N'Bangla & English', N'Bangla & English')
SET IDENTITY_INSERT [dbo].[SchoolVersion] OFF

go
--03
SET IDENTITY_INSERT [dbo].[Quota] ON 

INSERT [dbo].[Quota] ([Id], [QuotaName]) VALUES (1, N'Freedom Fighter')
INSERT [dbo].[Quota] ([Id], [QuotaName]) VALUES (2, N'Physically Challenged')
INSERT [dbo].[Quota] ([Id], [QuotaName]) VALUES (3, N'BNCC')
INSERT [dbo].[Quota] ([Id], [QuotaName]) VALUES (4, N'Tribal')
INSERT [dbo].[Quota] ([Id], [QuotaName]) VALUES (5, N'Ansar-VDP')
INSERT [dbo].[Quota] ([Id], [QuotaName]) VALUES (6, N'Orphan')
INSERT [dbo].[Quota] ([Id], [QuotaName]) VALUES (7, N'Femail')
SET IDENTITY_INSERT [dbo].[Quota] OFF
go
--04
SET IDENTITY_INSERT [dbo].[Room] ON 

INSERT [dbo].[Room] ([Id], [RoomName], [SitCapacity],[BrunchId]) VALUES (1, N'101',60, 1)
INSERT [dbo].[Room] ([Id], [RoomName], [SitCapacity], [BrunchId]) VALUES (2, N'102',60, 1)
INSERT [dbo].[Room] ([Id], [RoomName], [SitCapacity], [BrunchId]) VALUES (3, N'103',60, 1)
INSERT [dbo].[Room] ([Id], [RoomName], [SitCapacity], [BrunchId]) VALUES (4, N'104', 60,1)
INSERT [dbo].[Room] ([Id], [RoomName], [SitCapacity], [BrunchId]) VALUES (5, N'201',60, 2)
INSERT [dbo].[Room] ([Id], [RoomName], [SitCapacity], [BrunchId]) VALUES (6, N'202',60, 2)
INSERT [dbo].[Room] ([Id], [RoomName], [SitCapacity], [BrunchId]) VALUES (7, N'203',60, 2)
SET IDENTITY_INSERT [dbo].[Room] OFF
go
--05
SET IDENTITY_INSERT [dbo].[SchoolClass] ON 

INSERT [dbo].[SchoolClass] ([Id], [ClassName]) VALUES (1, N'One')
INSERT [dbo].[SchoolClass] ([Id], [ClassName]) VALUES (2, N'Two')
INSERT [dbo].[SchoolClass] ([Id], [ClassName]) VALUES (3, N'Three')
INSERT [dbo].[SchoolClass] ([Id], [ClassName]) VALUES (4, N'Four')
INSERT [dbo].[SchoolClass] ([Id], [ClassName]) VALUES (5, N'Five')
INSERT [dbo].[SchoolClass] ([Id], [ClassName]) VALUES (6, N'Six')
INSERT [dbo].[SchoolClass] ([Id], [ClassName]) VALUES (7, N'Seven')
INSERT [dbo].[SchoolClass] ([Id], [ClassName]) VALUES (8, N'Eight')
INSERT [dbo].[SchoolClass] ([Id], [ClassName]) VALUES (9, N'Nine')
INSERT [dbo].[SchoolClass] ([Id], [ClassName]) VALUES (10, N'Ten')
SET IDENTITY_INSERT [dbo].[SchoolClass] OFF
go
--06
SET IDENTITY_INSERT [dbo].[Shift] ON 

INSERT [dbo].[Shift] ([Id], [ShiftName]) VALUES (1, N'Morning')
INSERT [dbo].[Shift] ([Id], [ShiftName]) VALUES (2, N'Day')
INSERT [dbo].[Shift] ([Id], [ShiftName]) VALUES (3, N'Evening')
SET IDENTITY_INSERT [dbo].[Shift] OFF
go
--07
SET IDENTITY_INSERT [dbo].[EducationSystem] ON 

INSERT [dbo].[EducationSystem] ([Id], [Name], [StudentType], [BrunchId], [SchoolVersionId], [ShiftId]) VALUES (1, N'primary', 2, 1, 1, 1)
INSERT [dbo].[EducationSystem] ([Id], [Name], [StudentType], [BrunchId], [SchoolVersionId], [ShiftId]) VALUES (3, N'primary', 2, 1, 1, 2)
INSERT [dbo].[EducationSystem] ([Id], [Name], [StudentType], [BrunchId], [SchoolVersionId], [ShiftId]) VALUES (4, N'jonior', 2, 2, 2, 1)
INSERT [dbo].[EducationSystem] ([Id], [Name], [StudentType], [BrunchId], [SchoolVersionId], [ShiftId]) VALUES (5, N'jonior', 2, 2, 2, 2)
SET IDENTITY_INSERT [dbo].[EducationSystem] OFF


GO
--08
SET IDENTITY_INSERT [dbo].[SubjectGroup] ON 

INSERT [dbo].[SubjectGroup] ([Id], [GroupName]) VALUES (1, N'Business Studies')
INSERT [dbo].[SubjectGroup] ([Id], [GroupName]) VALUES (2, N'Humanities')
INSERT [dbo].[SubjectGroup] ([Id], [GroupName]) VALUES (3, N'Science')
INSERT [dbo].[SubjectGroup] ([Id], [GroupName]) VALUES (4, N'General')
SET IDENTITY_INSERT [dbo].[SubjectGroup] OFF

GO
--09
SET IDENTITY_INSERT [dbo].[Section] ON 

INSERT [dbo].[Section] ([Id], [SectionName], [SubjectGroupId], [SchoolClassId]) VALUES (1, N'A', 4, 1)
INSERT [dbo].[Section] ([Id], [SectionName], [SubjectGroupId], [SchoolClassId]) VALUES (2, N'B', 4, 2)
INSERT [dbo].[Section] ([Id], [SectionName], [SubjectGroupId], [SchoolClassId]) VALUES (3, N'A', 1, 9)
INSERT [dbo].[Section] ([Id], [SectionName], [SubjectGroupId], [SchoolClassId]) VALUES (4, N'B', 2, 9)
INSERT [dbo].[Section] ([Id], [SectionName], [SubjectGroupId], [SchoolClassId]) VALUES (5, N'C', 3, 9)
SET IDENTITY_INSERT [dbo].[Section] OFF
go
--10
SET IDENTITY_INSERT [dbo].[Subject] ON 

INSERT [dbo].[Subject] ([Id], [SubjectName], [SubjectCode], [SubjectGroupId]) VALUES (1, N'Bangla', N'101', 4)
INSERT [dbo].[Subject] ([Id], [SubjectName], [SubjectCode], [SubjectGroupId]) VALUES (2, N'English', N'102', 4)
INSERT [dbo].[Subject] ([Id], [SubjectName], [SubjectCode], [SubjectGroupId]) VALUES (3, N'Accounting', N'500', 1)
INSERT [dbo].[Subject] ([Id], [SubjectName], [SubjectCode], [SubjectGroupId]) VALUES (4, N'History', N'150', 2)
INSERT [dbo].[Subject] ([Id], [SubjectName], [SubjectCode], [SubjectGroupId]) VALUES (5, N'Higher Math', N'009', 3)
SET IDENTITY_INSERT [dbo].[Subject] OFF
go
--11
SET IDENTITY_INSERT [dbo].[Designation] ON 

INSERT [dbo].[Designation] ([Id], [DesignationName]) VALUES (1, N'Authority')
INSERT [dbo].[Designation] ([Id], [DesignationName]) VALUES (2, N'Principal')
INSERT [dbo].[Designation] ([Id], [DesignationName]) VALUES (3, N'Vice-Principal')
INSERT [dbo].[Designation] ([Id], [DesignationName]) VALUES (4, N'Assistant Teacher')
INSERT [dbo].[Designation] ([Id], [DesignationName]) VALUES (5, N'Jonior Teacher')
INSERT [dbo].[Designation] ([Id], [DesignationName]) VALUES (6, N'Office Assistant')
INSERT [dbo].[Designation] ([Id], [DesignationName]) VALUES (7, N'Office Support Stap')
INSERT [dbo].[Designation] ([Id], [DesignationName]) VALUES (8, N'Libraryan')
INSERT [dbo].[Designation] ([Id], [DesignationName]) VALUES (9, N'Gard')
INSERT [dbo].[Designation] ([Id], [DesignationName]) VALUES (10, N'Cleaner')
SET IDENTITY_INSERT [dbo].[Designation] OFF
GO
--12
SET IDENTITY_INSERT [dbo].[Guardian] ON 

INSERT [dbo].[Guardian] ([Id], [FirstName], [LastName], [DateOfBirth], [PasswordHint], [Gender], [Religion], [PhoneNo], [Email], [NidNo], [ImageUrl], [CurrentStatus], [PresentAddress], [ParmanentAddress], [CountryId], [DistrictId], [DivisionId], [PoliceStationId], [PostOfficeId], [AltGuardianName], [RelationOfAltGuardian], [AltGuardianPhoneNo], [AltGuardianEmail]) VALUES (1, N'Korim', N'Ahmed', CAST(N'1960-08-23T00:00:00.0000000' AS DateTime2), N'number', 1, 1, N'0195243254', N'Guardian@gmail.com', N'56456423184', N'~/images/3.jpg', NULL, N'Dhaka', N'Dhaka', 1, 5, 1, 2, 1, N'Ahsan', N'Brother', N'01973542454', N'alt@gmail.com')
INSERT [dbo].[Guardian] ([Id], [FirstName], [LastName], [DateOfBirth], [PasswordHint], [Gender], [Religion], [PhoneNo], [Email], [NidNo], [ImageUrl], [CurrentStatus], [PresentAddress], [ParmanentAddress], [CountryId], [DistrictId], [DivisionId], [PoliceStationId], [PostOfficeId], [AltGuardianName], [RelationOfAltGuardian], [AltGuardianPhoneNo], [AltGuardianEmail]) VALUES (2, N'Afroja', N'Begum', CAST(N'1980-09-30T00:00:00.0000000' AS DateTime2), N'number', 2, 1, N'06456512120', N'af@gmail.com', N'54674654', N'~/images/4.jpg', NULL, N'Barisal', N'Dhaka', 1, 2, 4, 1, 1, N'kawser', N'Ankel', N'017645821414', N'ka@gmail.com')
SET IDENTITY_INSERT [dbo].[Guardian] OFF
go
--13
SET IDENTITY_INSERT [dbo].[Students] ON 

INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DateOfBirth], [PasswordHint], [Gender], [Religion], [PhoneNo], [Email], [NidNo], [ImageUrl], [CurrentStatus], [PresentAddress], [ParmanentAddress], [CountryId], [DistrictId], [DivisionId], [PoliceStationId], [PostOfficeId], [FatherName], [FatherOccupation], [FatherPhoneNo], [MotherName], [MotherOccupation], [MotherPhone], [FormarSchoolName], [AdmissionDate], [SchoolClassId], [QuotaId], [GuardianId], [RegistrationNo], [RollNo]) VALUES (1, N'Robiul', N'Hossain', CAST(N'1995-02-10T00:00:00.0000000' AS DateTime2), N'number', 1, 1, N'0184563211', N'rh@gmail.com', N'65452165458', N'~/images/5.jpg', N'Active', N'Dhaka', N'Comilla', 1, 4, 2, 3, 33, N'Abul', N'Private Employee', N'01644846545', N'Razia Begum', N'houseWife', N'013654524545', NULL, CAST(N'2020-06-20T00:00:00.0000000' AS DateTime2), 9, NULL, 1, 12345, 111)
INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DateOfBirth], [PasswordHint], [Gender], [Religion], [PhoneNo], [Email], [NidNo], [ImageUrl], [CurrentStatus], [PresentAddress], [ParmanentAddress], [CountryId], [DistrictId], [DivisionId], [PoliceStationId], [PostOfficeId], [FatherName], [FatherOccupation], [FatherPhoneNo], [MotherName], [MotherOccupation], [MotherPhone], [FormarSchoolName], [AdmissionDate], [SchoolClassId], [QuotaId], [GuardianId], [RegistrationNo], [RollNo]) VALUES (2, N'Rokiya', N'Akter', CAST(N'1996-03-06T00:00:00.0000000' AS DateTime2), N'number', 2, 1, N'0123456987', N'rokiya@gmail.com', N'564241546456', N'~/images/6.jpg', N'regular', N'Meghna', N'Meghna', 1, 2, 3, 1, 10, N'Ahmed', N'Privet Employee', N'01644528547', N'Mother', N'House Wife', N'013654158745', NULL, CAST(N'2020-03-12T00:00:00.0000000' AS DateTime2), 8, 5, 2, 12346, 222)
SET IDENTITY_INSERT [dbo].[Students] OFF
go
--14
SET IDENTITY_INSERT [dbo].[AdmissionApply] ON 

INSERT [dbo].[AdmissionApply] ([Id], [FirstName], [LastName], [DateOfBirth], [PasswordHint], [Gender], [Religion], [PhoneNo], [Email], [NidNo], [ImageUrl], [CurrentStatus], [PresentAddress], [ParmanentAddress], [CountryId], [DistrictId], [DivisionId], [PoliceStationId], [PostOfficeId], [FatherName], [FatherOccupation], [FatherPhone], [MotherName], [MotherOccupation], [MotherPhone], [FormarSchoolName], [ApplingDate], [SchoolClassId], [QuotaId]) VALUES (1, N'Imran', N'Hossain', CAST(N'1996-06-10T00:00:00.0000000' AS DateTime2), N'UUU', 1, 1, N'01365648214', N'imran@gmail.com', N'56464521984', N'~/images/9.jpg', NULL, N'Mugda', N'Barisal', 1, 4, 2, 45, 41, N'Father', N'Govt', N'0123554254', N'mother', N'Houe Wife', N'013654185', NULL, CAST(N'2020-07-20T00:00:00.0000000' AS DateTime2), 6, NULL)
INSERT [dbo].[AdmissionApply] ([Id], [FirstName], [LastName], [DateOfBirth], [PasswordHint], [Gender], [Religion], [PhoneNo], [Email], [NidNo], [ImageUrl], [CurrentStatus], [PresentAddress], [ParmanentAddress], [CountryId], [DistrictId], [DivisionId], [PoliceStationId], [PostOfficeId], [FatherName], [FatherOccupation], [FatherPhone], [MotherName], [MotherOccupation], [MotherPhone], [FormarSchoolName], [ApplingDate], [SchoolClassId], [QuotaId]) VALUES (2, N'Saleha', N'Akter', CAST(N'1997-02-04T00:00:00.0000000' AS DateTime2), N'sss', 2, 1, N'01245632365', N'saleha@gmail.com', N'52454566547', N'~/images/10.jpg', NULL, N'Rangpur', N'Dhaka', 1, 2, 4, 40, 10, N'father', N'private', N'0185463126', N'mother', N'Banker', N'019654841', N'old School', CAST(N'2020-01-01T00:00:00.0000000' AS DateTime2), 8, 4)
SET IDENTITY_INSERT [dbo].[AdmissionApply] OFF
go
--15
SET IDENTITY_INSERT [dbo].[Staff] ON 

INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [DateOfBirth], [PasswordHint], [Gender], [Religion], [PhoneNo], [Email], [NidNo], [ImageUrl], [CurrentStatus], [PresentAddress], [ParmanentAddress], [CountryId], [DistrictId], [DivisionId], [PoliceStationId], [PostOfficeId], [FathersName], [MothersName], [JoingDate], [ResignDate], [BrunchId], [DesignationId]) VALUES (1, N'Josim', N'Ahmed', CAST(N'1978-06-10T00:00:00.0000000' AS DateTime2), N'A-Z', 1, 1, N'018546321455', N'Js@gmail.com', N'4655624321654', N'~/images/7.jpg', N'regular', N'Dhaka', N'Keranigang', 1, 2, 1, 45, 120, N'father', N'mother', CAST(N'2020-08-30T00:00:00.0000000' AS DateTime2), NULL, 1, 6)
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [DateOfBirth], [PasswordHint], [Gender], [Religion], [PhoneNo], [Email], [NidNo], [ImageUrl], [CurrentStatus], [PresentAddress], [ParmanentAddress], [CountryId], [DistrictId], [DivisionId], [PoliceStationId], [PostOfficeId], [FathersName], [MothersName], [JoingDate], [ResignDate], [BrunchId], [DesignationId]) VALUES (2, N'Anik ', N'Ahmed', CAST(N'1980-03-20T00:00:00.0000000' AS DateTime2), N'A-T', 1, 1, N'013025421521', N'anik@gmail.com', N'8546514684584', N'~/images/8.jpg', N'regular', N'Chittagong', N'Narayngang', 1, 8, 2, 12, 100, N'father', N'mother', CAST(N'2020-04-12T00:00:00.0000000' AS DateTime2), NULL, 2, 7)
SET IDENTITY_INSERT [dbo].[Staff] OFF
go
--16
SET IDENTITY_INSERT [dbo].[Teacher] ON 

INSERT [dbo].[Teacher] ([Id], [FirstName], [LastName], [DateOfBirth], [PasswordHint], [Gender], [Religion], [PhoneNo], [Email], [NidNo], [ImageUrl], [CurrentStatus], [PresentAddress], [ParmanentAddress], [CountryId], [DistrictId], [DivisionId], [PoliceStationId], [PostOfficeId], [MarritialStatus], [FathersName], [MothersName], [BrunchId], [DesignationId], [SubjectId]) VALUES (1, N'Jahangir', N'Hossain', CAST(N'1980-06-23T00:00:00.0000000' AS DateTime2), N'number', 1, 1, N'0181478524', N'jaha@gmail', N'545465941526', N'~/imagers/1.jpg', N'Ative', N'Dhaka', N'Rangpur', 1, 1, 1, 1, 1, 0, N'Jaha', N'motheer', 1, 4, 1)
INSERT [dbo].[Teacher] ([Id], [FirstName], [LastName], [DateOfBirth], [PasswordHint], [Gender], [Religion], [PhoneNo], [Email], [NidNo], [ImageUrl], [CurrentStatus], [PresentAddress], [ParmanentAddress], [CountryId], [DistrictId], [DivisionId], [PoliceStationId], [PostOfficeId], [MarritialStatus], [FathersName], [MothersName], [BrunchId], [DesignationId], [SubjectId]) VALUES (2, N'jahid', N'Hasan', CAST(N'1975-06-18T00:00:00.0000000' AS DateTime2), N'A-Z', 1, 1, N'015426554', N'jahid@gmail.com', N'56745841654', N'~/images/2.jpg', N'Consultant', N'Dhaka', N'Comilla', 1, 2, 2, 2, 2, 1, N'JahidF', N'Mother', 2, 3, 2)
SET IDENTITY_INSERT [dbo].[Teacher] OFF
GO
--17
SET IDENTITY_INSERT [dbo].[ClassRoom] ON 

INSERT [dbo].[ClassRoom] ([Id], [SchoolClassId], [RoomId], [SectionId], [ShiftId]) VALUES (1, 1, 1, 1, 1)
INSERT [dbo].[ClassRoom] ([Id], [SchoolClassId], [RoomId], [SectionId], [ShiftId]) VALUES (2, 1, 2, 1, 2)
INSERT [dbo].[ClassRoom] ([Id], [SchoolClassId], [RoomId], [SectionId], [ShiftId]) VALUES (3, 2, 3, 2, 3)
INSERT [dbo].[ClassRoom] ([Id], [SchoolClassId], [RoomId], [SectionId], [ShiftId]) VALUES (4, 3, 2, 4, 2)
INSERT [dbo].[ClassRoom] ([Id], [SchoolClassId], [RoomId], [SectionId], [ShiftId]) VALUES (5, 6, 5, 3, 1)
SET IDENTITY_INSERT [dbo].[ClassRoom] OFF
GO
--18
SET IDENTITY_INSERT [dbo].[ClassRoutine] ON 

INSERT [dbo].[ClassRoutine] ([Id], [StartTime], [EndTime], [ClassDueation], [PeriodNumber], [SubjectId], [TeacherId], [ClassRoomId], [DayOfWeek]) VALUES (4, CAST(N'08:00:00' AS Time), CAST(N'08:40:00' AS Time), N'40', 1, 1, 1, 1, 3)
INSERT [dbo].[ClassRoutine] ([Id], [StartTime], [EndTime], [ClassDueation], [PeriodNumber], [SubjectId], [TeacherId], [ClassRoomId], [DayOfWeek]) VALUES (6, CAST(N'08:40:00' AS Time), CAST(N'09:20:00' AS Time), N'40', 2, 2, 2, 2, 3)
SET IDENTITY_INSERT [dbo].[ClassRoutine] OFF
go
--19
SET IDENTITY_INSERT [dbo].[Exam] ON 

INSERT [dbo].[Exam] ([Id], [ExamType], [ExamDiscription], [StartDate], [EndDate], [Duration], [BrunchId]) VALUES (1, N'Half Yearly', N'this is 50 mark & class under 8 ', CAST(N'2020-09-10T00:00:00.0000000' AS DateTime2), CAST(N'2020-10-30T00:00:00.0000000' AS DateTime2), N'3 Hour', 1)
INSERT [dbo].[Exam] ([Id], [ExamType], [ExamDiscription], [StartDate], [EndDate], [Duration], [BrunchId]) VALUES (2, N'Yearly', N'100 marks', CAST(N'2020-12-01T00:00:00.0000000' AS DateTime2), CAST(N'2020-12-31T00:00:00.0000000' AS DateTime2), N'3 Hour', 1)
SET IDENTITY_INSERT [dbo].[Exam] OFF
go
--20
SET IDENTITY_INSERT [dbo].[ExamRoutine] ON 

INSERT [dbo].[ExamRoutine] ([Id], [ExamDate], [ActiveStatus], [SchoolClassId], [ExamId], [ShiftId], [SubjectId]) VALUES (1, CAST(N'2020-09-03T00:00:00.0000000' AS DateTime2), 1, 1, 1, 2, 1)
INSERT [dbo].[ExamRoutine] ([Id], [ExamDate], [ActiveStatus], [SchoolClassId], [ExamId], [ShiftId], [SubjectId]) VALUES (2, CAST(N'2020-09-03T00:00:00.0000000' AS DateTime2), 1, 2, 1, 2, 2)
SET IDENTITY_INSERT [dbo].[ExamRoutine] OFF
go
--21
SET IDENTITY_INSERT [dbo].[ExamResult] ON 

INSERT [dbo].[ExamResult] ([Id], [ExamPublishDate], [TotalScore], [PassScore], [ObtainScore], [HighScore], [GrandTotalScore], [LatterGrade], [GradePoint], [Position], [ResultStatus], [TotalPresent], [TotalAbsent], [Note], [SchoolClassId], [ExamId], [StudentId], [SubjectId]) VALUES (2, CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), 100, 50, 70, 95, 652, N'A', 4.25, 33, 1, 10, 0, N'Study More', 3, 1, 1, 1)
INSERT [dbo].[ExamResult] ([Id], [ExamPublishDate], [TotalScore], [PassScore], [ObtainScore], [HighScore], [GrandTotalScore], [LatterGrade], [GradePoint], [Position], [ResultStatus], [TotalPresent], [TotalAbsent], [Note], [SchoolClassId], [ExamId], [StudentId], [SubjectId]) VALUES (4, CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), 100, 50, 65, 95, 700, N'A', 4.36, 12, 1, 10, 0, N'rest new', 3, 1, 1, 2)
SET IDENTITY_INSERT [dbo].[ExamResult] OFF
go
--22
SET IDENTITY_INSERT [dbo].[Event] ON 

INSERT [dbo].[Event] ([Id], [EventName], [StartDate], [EndDate], [EventControlar], [ImageUrl], [BrunchId]) VALUES (1, N'Orentation', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), N'Khalid', N'~/images/11.jpg', 1)
INSERT [dbo].[Event] ([Id], [EventName], [StartDate], [EndDate], [EventControlar], [ImageUrl], [BrunchId]) VALUES (2, N'Farewell', CAST(N'2020-12-31T00:00:00.0000000' AS DateTime2), CAST(N'2020-12-31T00:00:00.0000000' AS DateTime2), N'Jahangir', N'~/images/12.jpg', 1)
SET IDENTITY_INSERT [dbo].[Event] OFF

go
--23

SET IDENTITY_INSERT [dbo].[Holiday] ON 

INSERT [dbo].[Holiday] ([Id], [HolidayName], [StartDate], [EndDate], [NumberOfDay], [BrunchId]) VALUES (1, N'Summer Session', CAST(N'2021-02-01T00:00:00.0000000' AS DateTime2), CAST(N'2021-02-10T00:00:00.0000000' AS DateTime2), 10, 2)
INSERT [dbo].[Holiday] ([Id], [HolidayName], [StartDate], [EndDate], [NumberOfDay], [BrunchId]) VALUES (2, N'Eid Ul Azha', CAST(N'2020-06-01T00:00:00.0000000' AS DateTime2), CAST(N'2020-06-06T00:00:00.0000000' AS DateTime2), 6, 1)
SET IDENTITY_INSERT [dbo].[Holiday] OFF

go
--24
SET IDENTITY_INSERT [dbo].[RulesRegulations] ON 

INSERT [dbo].[RulesRegulations] ([Id], [RuleDetails], [BrunchId]) VALUES (1, N'Must be use Unifrom. Attend Correct Time', 1)
INSERT [dbo].[RulesRegulations] ([Id], [RuleDetails], [BrunchId]) VALUES (2, N'Must be use Unifrom. Attend Correct Time', 2)
SET IDENTITY_INSERT [dbo].[RulesRegulations] OFF

go
--25
SET IDENTITY_INSERT [dbo].[NoticeBoard] ON 

INSERT [dbo].[NoticeBoard] ([Id], [TopicName], [NoticeBody], [PublishDate], [BrunchId], [SchoolClassId]) VALUES (1, N'Exam', N'2nd september will held montly Exam', CAST(N'2020-08-31T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[NoticeBoard] ([Id], [TopicName], [NoticeBody], [PublishDate], [BrunchId], [SchoolClassId]) VALUES (2, N'Result', N'Final Result', CAST(N'2020-12-31T00:00:00.0000000' AS DateTime2), 1, 1)
SET IDENTITY_INSERT [dbo].[NoticeBoard] OFF

